---
layout: page
title: Index
order: 1
---




# MATERIALS for TESTING INSTANCE  

## Projects

|            |Single CellDesigner map                                                                                           |single CellDesigner map                                                                        |advanced CellDesigner map                                                                                                |SBGN-ML PD map (displayed as SBGN)                                                                                       | empty |
|---------------|------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------|---------|
|Canvas         |Open Layers                                                                                                       |GoogleMaps                                                                                                         |Open Layers                                                                                                              |Open Layers                                                                                                              |         |
|Overlays       | 3 single overlays                                                                                                             | 3 single overlays                                                                                                              |inside layout dictionary                                                                                                 |none                                                                                                                     |         |
|Project Id     |single-map                                                                                                        | singleGM-map                                                                                                      |advanced-map                                                                                                             |SBGN-ML-map                                                                                                              |         |
|Project Name   |CellD in OpenLayers                                                                                               |CellD in GoogleMaps                                                                                                | Advanced map                                                                                                            |From NEWT map                                                                                                            |         |
|Project Disease|D010300                                                                                                           |D010300                                                                                                            |D010300                                                                                                                  |D010300                                                                                                                  |         |
|Organism       |9606                                                                                                              |9606                                                                                                               |9606                                                                                                                     |9606                                                                                                                     |         |
|file           |[sourceMaps/singleTestMap.zip](https://git-r3lab.uni.lu/ewa.smula/testing/raw/master/sourceMaps/singleTestMap.zip)| [sourceMaps/singleTestMap.zip](https://git-r3lab.uni.lu/ewa.smula/testing/raw/master/sourceMaps/singleTestMap.zip)|[sourceMaps/advancedMap.zip](https://git-r3lab.uni.lu/ewa.smula/testing/raw/master/sourceMaps/advancedMap.zip)|[sourceMaps/SBGNmap.sbgn](https://git-r3lab.uni.lu/ewa.smula/testing/raw/master/sourceMaps/SBGNmap.sbgn)|         |




<!-- 1.  single CellDesigner map (Open Layers) with three overlays
    * Project Id: single-map
    * Project Name: CellD in OpenLayers
    * Project Disease: D010300
    * Organism: 9606
    * file: [sourceMaps/singleTestMap.zip](https://git-r3lab.uni.lu/ewa.smula/testing/raw/master/sourceMaps/singleTestMap.zip)

2.  the same map as in 1. : single CellDesigner map (GoogleMaps) with three overlays
    * Project Id: singleGM-map
    * Project Name: CellD in GoogleMaps
    * Project Disease: D010300
    * Organism: 9606
    * file: [sourceMaps/singleTestMap.zip](https://git-r3lab.uni.lu/ewa.smula/testing/raw/master/sourceMaps/singleTestMap.zip)

3.  advanced CellDesigner map
    * Project Id: advanced-map
    * Project Name: Advanced map
    * Project Disease: D010300
    * Organism: 9606
    * file: [sourceMaps/advancedMap.zip](https://git-r3lab.uni.lu/ewa.smula/testing/raw/master/sourceMaps/sourceMaps/advancedMap.zip)

4.  SBGN-ML PD map (displayed as SBGN)
    * Project Id: SBGN-ML-map
    * Project Name: From NEWT map
    * Project Disease: D010300
    * Organism: 9606
    * file: [sourceMaps/SBGN-MLmap.sbgn](https://git-r3lab.uni.lu/ewa.smula/testing/raw/master/sourceMaps/sourceMaps/SBGN-MLmap.sbgn)

5.  empty -->


<br>

### Comments on maps  

|          | comment 1               | comment 2           | comment 3             | comment 4               |
| --- | --- | --- |  --- |  --- |
| ProjectID | single-map              | single-map          | advanced-map (submap) | advanced-map (main map) |
| Type      | phenotype: phagocytosis | reaction:re100      | protein:AQP8          | reaction:re332          |
| Pinned    | N                       | Y                   | Y                     | Y                       |
| Name      | TEST user               | TEST user           | TEST user             | TEST user               |
| Content   | There is a comment!     | There is a comment! | There is a comment!   | There is a comment!     |


<br>

## Users details

|          | 1st admin            | 2nd admin        | Curator            | User-can-create-overlays  | User             |
|----------|----------------------|------------------|--------------------|---------------------------|------------------|
| login    | test-admin           | test-general     | test-curator       | test-user-overlay         | test-user        |
| password | x                    | x                | x                  | x                         | x                |
| name     | TEST-admin           | TEST-general     | TEST-curator       | TEST-user-overlay         | TEST-user        |
| surname  | Adam                 | Eastwood         | BruceLee           | Kaz                       | Unicorn          |
| email    | ewa.smula@uni.lu     | ewa.smula@uni.lu |   ewa.smula@uni.lu | ewa.smula@uni.lu          | ewa.smula@uni.lu |


<br>

### Users access to the maps

 * Privileges for:
    * curator,
    * user-can-create-overlays,
    * user

| project id  | view | modify |
| --- | --- | --- |
| single | Y | Y |
| singleGM | Y | Y |
| advanced | Y | N |
| SBGNML-map | Y | N |
| empty | N | N |

<br>

  * Privileges for:
    * anonymous

| project id  | view | modify |
| --- | --- | --- |
| single | Y | N |
| singleGM | Y | N |
| advanced | Y | N |
| SBGNML-map | Y | N |
| empty | N | N |



<br>

### GENOMES upload (not very important)
